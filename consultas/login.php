<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body>
    <?php
    session_start();
    include("../conexion/conexion.php");
    $correo = $_POST['correo'];
    $contrasena1 = $_POST['contrasena'];
    $contrasena = md5($_POST['contrasena']);
    $ip = 'login';
    if (empty($correo)) {
        echo json_encode(["message" => "Ingrese el Correo Electronico"]);
    } else if (empty($contrasena1)) {
        echo json_encode(["message" => "Ingrese la contraseña"]);
    } else if ($ip == "login") {
        $sql = "SELECT c.Correo,c.Contrasena,
(a.Id)AS Id,
(a.Cedula)AS Cedula,
(a.Nombres)AS Nombres,
(a.Apellidos)AS Apellidos,
(a.Telefono)AS Telefono,
(a.Direccion)AS Direccion,
(a.Nombres_completos)AS Nomb_compl,
(a.Tipo)AS Tipo
FROM cuenta_admin c 
INNER JOIN administrativos a
ON c.Administrativos=a.Id
WHERE c.Correo = '$correo' and a.Estado = '1'";
        $res = $obj_conexion->query($sql);
        if ($pass = $res->fetch_assoc()) {
            if ($contrasena == $pass['Contrasena']) {
                $Usuario = $_SESSION['Nombres'] = $pass['Nomb_compl'];
                $_SESSION['loggedin'] = true;
                $_SESSION['correo'] = $pass['Correo'];
                $_SESSION['Nombres'] = $pass['Nomb_compl'];
                $_SESSION['Tipo'] = $pass['Tipo'];
                $href = '../Administrador';
                echo "<script> 
        Swal.fire({
          icon: 'success',
          title: 'Bienvenido, $Usuario',
          position: 'center',
          showConfirmButton: false
        })
        setTimeout(function (){location.href='$href'},1000); 
      </script>";
            } else {
                echo "<script> 
                  Swal.fire({
                    icon: 'error',
                    title: 'CREDENCIALES INCORRECTAS',
                    position: 'center',
                    showConfirmButton: false
                  })
                  setTimeout(function (){location.href='../index.php'},1000); 
                </script>";
            }
        } else {
            echo "<script> 
              Swal.fire({
                icon: 'warning',
                title: 'USUARIO NO REGISTRADO',
                position: 'center',
                showConfirmButton: false
              })
              setTimeout(function (){location.href='../index.php'},1000); 
            </script>";
        }
    } else {
        echo $ip . "<script> location.href='../index.php'</script>";
    }
    ?>
</body>

</html>