<?php
include("conexion/conexion.php");
include("menus/encabezdos.php");
$fechaActual = date('Y-m-d');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
	<link rel="icon" href="img/logo.gif" type="image/ico" />
	<title><?= $titulo ?></title>
	<style>
		* {
			margin: 0;
			padding: 0;
		}

		#particles-js {
			height: 100vh;
			width: 100%;
			background: #374a5e;
			position: fixed;
			z-index: -1;
		}

		#contenedor {
			position: relative;
			z-index: 99;
		}
	</style>
</head>

<body>
	<div id="particles-js"></div>
	<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
		<div class="card card0 border-0">
			<div class="row d-flex">
				<div class="col-lg-6">
					<div class="card1 pb-5">
						<br><br>
						<div class="row px-3 justify-content-center mt-4 mb-5 border-line">
							<img src="https://acegif.com/wp-content/uploads/gifs/water-43.gif" class="image">
						</div>
					</div>
				</div>
				
					<div class="col-lg-6">
					<form class="login100-form validate-form flex-sb flex-w" method="post" action="consultas/login.php">
						<div class="card2 card border-0 px-4 py-5">
							<div class="row mb-4 px-3">
								<h6 class="mb-0 mr-4 mt-2"><?= $titulo_login ?></h6>
							</div>
							<div class="row px-3 mb-4">
								<div class="line"></div>
								<small class="or text-center">Inicie session</small>
								<div class="line"></div>
							</div>
							<div class="row px-3">
								<label class="mb-1">
									<h6 class="mb-0 text-sm">Correo Electronico</h6>
								</label>
								<input class="mb-4" type="text" id="correo" name="correo" placeholder="Correo Electronico">
							</div>
							<div class="row px-3">
								<label class="mb-1">
									<h6 class="mb-0 text-sm">Contraseña</h6>
								</label>
								<input type="password" id="contrasena" name="contrasena" placeholder="************">
							</div>
							<div class="row px-3 mb-4">
								<a href="#" class="ml-auto mb-0 text-sm">Recuperar contraseña</a>
							</div>
							<div class="row mb-3 px-3">
								<button type="submit" class="btn btn-info text-center" id="ingresar_sistema">Ingresar</button>
							</div>

						</div>
						</form>
					</div>
				
			</div>
			<div class="bg-blue py-4">
				<div class="row px-3">
					<small class="ml-4 ml-sm-5 mb-2">Barrio Milagro &copy; <?= $fechaActual ?></small>

				</div>
			</div>
		</div>
	</div>
	<script src="js/particles.min.js"></script>
	<script src="js/particlesjs-config.json"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script>
		$(document).ready(function() {
			// $("#ingresar_sistema").click(function(e) {
			// 	var correo = $("#correo").val();
			// 	var contrasena = $("#contrasena").val();
			// 	$.ajax({
			// 		type: "post",
			// 		url: "consultas/login.php",
			// 		data: {
			// 			correo: correo,
			// 			contrasena: contrasena
			// 		},
			// 		dataType: "json",
			// 		beforeSend: function() {},
			// 		success: function(data) {
			// 			if (data.message) {
			// 				toastr.error(data.message);
			// 			} else {
			// 				toastr.success("BIENVENIDO");
			// 				setTimeout(function() {
			// 					window.location.href = "Administrador/index.php"
			// 				}, 1000);
			// 			}
			// 		},
			// 		complete: function(data) {},
			// 		error: function(jqXHR, exception) {
			// 			console.log(jqXHR.responseText);
			// 		},
			// 	});
			// });
		});
	</script>
</body>

</html>