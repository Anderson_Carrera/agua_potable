<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../img/logo.gif" type="image/ico" />
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <title>Cobro del Mes</title>
    <style>
        .loader {
            position: absolute;
            border: 16px solid #f3f3f3;
            border-radius: 350%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            width: 320px;
            height: 320px;
            z-index: 99;
            top: 25%;
            left: 50%;
            display: none;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <?php
    include("../conexion/conexion.php");
    ?>
</head>

<body class="nav-md">
    <div class="loader" id="loader_div"></div>
    <?php
    include("menu_opciones/registro_cobros.php");
    include("menu_opciones/footer.php");
    ?>

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <script src="vendors/nprogress/nprogress.js"></script>
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <script src="vendors/gauge.js/dist/gauge.min.js"></script>
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="vendors/iCheck/icheck.min.js"></script>
    <script src="vendors/skycons/skycons.js"></script>
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <script src="vendors/DateJS/build/date.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="build/js/custom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#nombre_usuarios").select2();
            $(document).click(function() { //Creamos la Funcion del Click
                    var checked = $(".CheckedAK:checked").length;
                    //Creamos una Variable y Obtenemos el Numero de Checkbox que esten Seleccionados
                    $("#resp").text("Tienes Actualmente (" + checked + ") Registros " + "Seleccionado(s)");
                    //Asignamos a la Etiqueta <p> el texto de cuantos Checkbox hay Seleccionados
                    if (checked == 0) {
                        $('#enviarform').hide(); //ocultar
                    } else {
                        $("#enviarform").fadeIn("slow"); //mostrar
                    }
                })
                .trigger("click");
            $(document).on('click keyup', '.CheckedAK,.mis-adicionales', function() {
                calcular();
            });

            $("#nombre_usuarios").change(function() {
                var id_usuario = $("#nombre_usuarios").val();
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: 'menu_opciones/basboard/consultas/consultar_meses_pendientes.php',
                    data: {
                        id_usuario: id_usuario
                    },
                    success: function(data) {
                        //console.log(data);
                        cargarTablaPlan(data);

                    },
                    error: function(jqXHR, exception) {
                        console.log(jqXHR.responseText);
                    },
                });
            });


        });

        function calcular() {
            var tot = $('#total');
            tot.val(0);
            $('.CheckedAK,.mis-adicionales').each(function() {
                if ($(this).hasClass('CheckedAK')) {
                    tot.val(($(this).is(':checked') ? parseFloat($(this).attr('tu-attr-precio')) : 0) + parseFloat(tot.val()));
                } else {
                    tot.val(parseFloat(tot.val()) + (isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val())));
                }
            });
            var totalParts = parseFloat(tot.val()).toFixed(2).split('.');
            tot.val('$' + totalParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.' + (totalParts.length > 1 ? totalParts[1] : '00'));
        }

        function cargarTablaPlan(data) {
            var table = $('#tbcobros_agua').DataTable();
            table.destroy();
            table.clear();
            $('#tbcobros_agua').dataTable({
                deferRender: true,
                data: data,
                dom: 'Bfrtip',
                searching: false,
                buttons: [

                ],
                language: {
                    "url": "https://cdn.datatables.net/plug-ins/1.13.1/i18n/es-ES.json"
                },
                columns: [{
                        visible: false,
                        render: function(data, type, full, meta) {
                            return (

                                ' <input type="text" class="form-control" id="id_usuario" value="' + full.Id + '"/>'

                            );
                        },
                    },
                    {
                        render: function(data, type, full, meta) {
                            return (
                                ' <input type="checkbox"  name="Contratos[]" class="CheckedAK" tu-attr-precio="' + full.Total + '" id="Contratos" value="' + full.Id + ',' + full.Exceso + ',' + full.Fecha_Ingresada + ',' + full.Usuarios + '"/>'
                            );
                        },
                    },
                    {
                        "data": "Fecha_Ingresada"
                    },
                    {
                        "data": "Usuario"
                    },
                    {
                        "data": "Lec_anterior"
                    },
                    {
                        "data": "Lec_actual"
                    },
                    {
                        "data": "Acometidas"
                    },
                    {
                        "data": "Exceso"
                    },
                ]
            });
        }

        function marcarCheckbox(source) {
            checkboxes = document.getElementsByTagName('input');
            //obtenemos todos los controles del tipo Input
            for (i = 0; i < checkboxes.length; i++)
            //recoremos todos los controles
            {
                if (checkboxes[i].type == "checkbox")
                //entramos si esta un checkbox
                {
                    checkboxes[i].checked = source.checked;
                    //si es un checkbox le damos el valor del checkbox
                }
            }
        }
    </script>
</body>

</html>