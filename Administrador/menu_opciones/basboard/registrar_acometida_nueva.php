<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>

<div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">
                <div class="container">
                    <hr>
                    <label>Ingrese el numero de acometidas por usuario</label>
                    <hr>
                    <button type="button" class="btn btn-info fa fa-floppy-o" id="btn_actualizar_acometida">
                        Actualizar Acometidas
                    </button>
                    <hr>
                    <table id="tbacometidas" class="table table-striped table-bordered" style="width: 100%;">
                        <thead class="bg-secondary">
                            <tr style="color: #fff;">
                                <th>Codigo</th>
                                <th>Cedula</th>
                                <th>Nombres Completos</th>
                                <th>Direccion</th>
                                <th>N.-Acometidas</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>