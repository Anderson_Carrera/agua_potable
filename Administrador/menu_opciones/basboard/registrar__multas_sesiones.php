<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>

<div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">
                <div class="container">
                    <hr>
                    <label>Ingreso de multas de las sesiones por los usuarios no asistidos EL VALOR DE LA MULTA ES DE 20 DOLARES</label>
                    <hr>
                    <button type="button" class="btn btn-info fa fa-floppy-o" id="btn_multas_mingas">
                        Ingresar Multas del Mes
                    </button>
                    <hr>
                    <div class="row">
                    <div class="col-md-2 ">
                    <label>Fecha de Ingreso : </label>
                    <input type="date" class="form-control" id="id_fecha">
                    </div>
                    
                    </div>
                    <hr>
                    <table id="tbsesiones" class="table table-striped table-bordered" style="width: 100%;">
                        <thead class="bg-secondary">
                            <tr style="color: #fff;">
                                <th>Codigo</th>
                                <th>Cedula</th>
                                <th>Nombres Completos</th>
                                <th>Direccion</th>
                                <th>Valor Multa ($20)</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>