<?php
date_default_timezone_set('America/Guayaquil');
$Fecha_Actual = date('Y-m-d', strtotime('-1 Month'));
$sql = "SELECT Id,Nombres_completos FROM usuarios WHERE Estado ='1'";
$resultado = $obj_conexion->query($sql);
?>
<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>

<div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">

                <div class="container">
                    <hr>
                    <label>Cobros de Mingas del agua potable del BARRIO MILAGRO</label>
                    <hr>
                    <div class="row">
                        <div class="col-md-3">
                            <select id="nombre_usuarios" class="form-control">
                                <option value="0">Seleccionar</option>
                                <?php
                                while ($row = $resultado->fetch_assoc()) {
                                ?>
                                    <option value="<?= $row['Id'] ?>"><?= $row['Nombres_completos'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <form action="menu_opciones/basboard/consultas/imprimir_factura_Mingas.php">
                        <div class="row">
                            <div class="col-md-3 ">
                                <label>MESES ADEUDADOS POR EL USUARIO :</label>
                            </div>
                            <div class="col-md-2 ">
                                <p id="resp"></p>
                            </div>
                            <!-- <div class="col-md-1 ">
                                <label>Mora</label>
                                <input id="Mora" name="Mora" type="text" class="mis-adicionales form-control" placeholder="0.00" value="0"/>
                            </div> -->
                            <div class="col-md-1 ">
                                <label>Total a Pagar</label>
                                <input id="total" name="total" type="text" class="form-control" placeholder="0.00" readonly />
                            </div>
                            <div class="col-md-2 ">
                                <input type="submit" name="enviarform" id="enviarform" class="btn btn-info" value="Generar el cobro">
                            </div>
                        </div>
                    
                    <hr>
                    <table id="tbcobros_Mingas" class="table table-striped table-bordered" style="width: 100%;">
                        <thead class="bg-secondary">
                            <tr style="color: #fff;">
                                <th>Codigo</th>
                                <th>Marcar</th>
                                <th>Usuario</th>
                                <th>Fecha Multa</th>
                                <th>Total Multa</th>
                            </tr>
                        </thead>
                    </table>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>