<?php
include("../../../../conexion/conexion.php");
require_once("mpdf/vendor/autoload.php");
date_default_timezone_set('America/Guayaquil');
$fecha_Hora = date('Y-m-d');
//$Mora = $_GET['Mora'];
$total = $_GET['total'];
if (isset($_REQUEST['enviarform'])) {
  if (is_array($_REQUEST['Contratos'])) {
    $num_countries = count($_REQUEST['Contratos']);
    $columna   = 1;
    //echo $num_countries;
    foreach ($_REQUEST['Contratos'] as $key => $emailCliente) {
      //$var_info = printf($emailCliente);
      $str_arr = explode(",", $emailCliente);
      $IdExceso = $str_arr[0];
      $exceso = $str_arr[1];
      $Meses_pago = $str_arr[2] . "--";
      $IdUsuario = $str_arr[3];
      $sql_update = "UPDATE exceso SET Fecha_cobro = '$fecha_Hora' , Estado = '2' WHERE Id = {$IdExceso}";
      $resultado = $obj_conexion->query($sql_update);
      $sql_usuario  = "SELECT e.Id,e.Usuarios,e.Fecha_Ingresada,e.Lec_anterior,e.Lec_actual,e.Exceso,
      (u.Nombres_completos)AS Nombres_completos,
      (u.Cedula)AS Cedula,
      (u.Telefono)AS Telefono,
      (u.Direccion)AS Direccion,
      (u.Correo)AS Correo,
      (u.Acometida)AS acometida
      FROM exceso e
      INNER JOIN usuarios u
      ON e.Usuarios = u.Id
      WHERE e.Id IN ($IdExceso) ";
      $resultado = $obj_conexion->query($sql_usuario);
      while ($row1 = mysqli_fetch_array($resultado)) {
        $fecha1 = $row1['Fecha_Ingresada'];
        $Nombres_completos = $row1['Nombres_completos'];
        $Direccion = $row1['Direccion'];
        $Telefono = $row1['Telefono'];
        $Lec_anterior = ($row1['Lec_anterior'] . "/");
        $Lec_actual = ($row1['Lec_actual'] . "/");
        $Fecha_Ingresada = ($row1['Fecha_Ingresada'] . "/");
        $Fecha_Ingresada_lista = $row1['Fecha_Ingresada'];
        $acometida = $row1['acometida'];
        $Exceso = $row1['Exceso'];
        $lecturas_anteriores .= '' . $Lec_anterior . '';
        $lecturas_actuales .= '' . $Lec_actual . '';
        $Fechas_Pago .= '' . $Fecha_Ingresada . '';
        $total_exesos = $acometida + $Exceso;
        $fechas_pago_1 .= '<tr>
        <th class="bordeGeneral textoCampos">' . $Fecha_Ingresada_lista . '</th>
        <th class="bordeGeneral textoCampos">' . $acometida . '</th>
        <th class="bordeGeneral textoCampos">' . $Exceso . '</th>
        <th class="bordeGeneral textoCampos">' . $total_exesos . '</th>
      </tr>';
      }
    }
    $output = '
    <style>
    .bordeGeneral {
      border: 1px solid #000000;
    }
    .alinearTexto {
      text-align:center;
    }
    .textoCampos {
      text-align:center;
      font-size:9px;
    }
    .textoInformacion {
      font-size:12px;
    }
    .textoGrande {
      font-weight:bold;
      font-size:16px;
    }
   </style>
   <table width="100%" cellpadding="5" cellspacing="0">
   <tr>
   <th rowspan=4 width="100%">
     <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFhYYGRgYGhocHBwcHBwdHhoaGRoaHRoaGhocIS4lHCErIRwaJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHzEsJCs2NDQ0PTQ0NjQ2NDQ2PTQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAEAAIDBQYHAf/EAEkQAAIBAgMEBwQHBgMFCQEAAAECAAMRBCExBRJBUQYiYXGBkaETMrHBFCNCYnLR8AdSkrLC4YKi0hUkM3PxNENTVGOTo7PiFv/EABoBAAMBAQEBAAAAAAAAAAAAAAECAwQABQb/xAAqEQACAgEDAwMEAgMAAAAAAAAAAQIRAxIhMQRBURMiMgUUYXGhsSMzgf/aAAwDAQACEQMRAD8AsUNpLeR7s9vAEdvRjGK8RnAGEySnGLHqJ1nEhjN2OvEDDZwBUZt9QwAFzmDqCCBl32kjJGYuzVUQ5jdY8e3l3CECnYWuTbidfGGL3C0D7saUhBSeFZRMSgfdiCSYrEEhsVohCRpEJ3ZFaMgEYWOVY/dj0WEAlWSok9VZIogYRyrHKs8EfeLQ1kq2jyRBt+0FrYkiFRbOcqCmYGQPUUcYA+LJGUAYuDfOUjj8kpTLl6wzlVtDGAaQXEY3dBHG0oauJYkm5zlseEhlzVsiy+nxSp9oeZil/TRn9WR0kCN3YgY688Sz2RlorR5k1ChvI7A5oQCO8A/MesVyS5OUW+AdRHRWiIjAPJFiEqMu7StvsVUEi4UEgM5HHdW7W42g+O2pRpZO4B/dGbeIGnjAsL0kp1BVVUc9QqthmzPZQAQerkTcki0E5OMW0NGNsusDgPZYd61d6m8gbeLBesqsd0gC1gwsQL5bxzjaL76q1rbwvY/rslh7BquD9mQxDDca5Bcjqm9xlkMtD3yM0jurlYAW5cSfnMeDJpm9T5NGSFx2QLuzzdku7PQs9JMx0DbsbaFlJEyRkwNEBMQS8mCSWjTzjpi0Ceznqw6sBICgjJ2K1QxY8RpFo5WhAegR1p4hhC04j2GSsEdSdIDWwrEay49naQ1HAhUvAHHyUlLCkHMyWowtaFOBK/GED8pVO2TaSRTbScW0zlQ00lXBO9rLy8e+WGE6Ob5UuFAGdlGvfNKyxhHcyywynLYxfszyMU6j/salyHkIpP7uPgp9nLyV6tJA0H3o9Wnl0ejY+pV3eBJ5D8zLfZWFL0HJA3nYnK/2QAAQbX4yjfFWsFUk7wTPIXNm97lYjyM2GxbeyWxuDvH/ADGednyy1UtqZqxwWmzOsu7feytrfhM7tXbm8y0cMyl2bdZwN8JlwC+8T5Dxmp6VYGnVemji4OZ7RvL5kc5jNhUvY0a9cgdRCRbIF95lQHxEp90nHjcCwW+dgPGbNdgypVQ7oJdGdN9myFwiX0vexPfJuiWGdHcM2QCWAyANzoNB/aZnBVSaiG+Zdc+85/EzY7KX61jwuB6Pl+uUd2otMXmSo6FsXOmO1m+KiEHBhs7kXN8tPI5QbYbfUp2lj/mlrTHVHdPNe8maFsiixeBKi5GXMaeI1HqICwtM9jelBNaurllszqjKTdN0kKGXQjLUZ5m9xpotl1fpFFHsFYkqy6dcC4K8gwsbcJsw5pY3UuCWTEpK1yMJjTJa1PdYrraRMZ6kN1aMctnTPVAj5AzxhqmWUSbkh1SpIrnnIK9WDriSchKxhsRc1ZYB48NKlsUFNjlH/S7G0b02BZEWquISlcTOUsfvMQNYQ+M3RrFlidjxyqrLOvXglRydJXVK5a3CLE4oKmWo4m8aOMSWUKaqdIxVUm5zlfhFdxv7rbvOxt58Zc4Chc2scp0lpDB6guikKSoRJ6WHsIqoUCZHK2aUqQ320UF9oIodINRXbRVEc7rXU6X18uI7RAWxqj++vgNTKnB4hqp6iu54hRYZ8WY5+OUt6GwHbNyEB1VPeP4n/KeW8+RR0vb+zcsUbvn+iuxO02Y7qqSeAtn2kIuZ7zabfZTtTpor5WA1sp62fPdbXgYDgtmU6a2RQL68z3k5nxmv9mNO1R5ATFnSaRaMqKnF0RUZXJIKggaWzvn6yixHR1votSijK7OykE9UWD71jrNamCTM7oGZOXV4niuc8OETg5HYSD6kX9ZFalvY+qPFHIR0SxVOojGk1lZSSpVhYHXqm/pLjZdJlqNvKQbiwIIOQbge/wBZ0U4Nxoyt5r/qjKlFuKEjssfTX0mh9TkqmkT9ON2mM2OLUk8f5mlkmg7pWpWVRu+7yUjd8gRC6VcaHKQUk5Ox3F0cN2q/19X/AJj/AM5nUOhNO+HW4BDopIOhIut5z/amyaitUquAoNR7LfrWZzYkWsOHG+c6L0K/7PT/AA/1Ga+oTUUTxtOx+16YV7j7Qvbkb5ysaoOcO6TPZk/B8zKIodbGez0kdWFSfg8zPKptIIatIalYc4+nhXYX3T5SB9nG+ZN/KbIqPkzycvAMahbIjjHexUWBYgnl84X9FVUsQb84KFG9xuP1xlFJdiTi+4I1AM1t+6rqbT3F4UEbyt5GW9HDIilvO/rpKbajFN1gosb6cjKRblLYnNKMXZULUIa41lvhSXt1bnjrK7D1gCbqD4ZzSYE3UW0jZnXYngV9yGmlrlxf5QhXDEKFuOVsh3ws0g0IoUAJjlkSN0cbY6hcLYWA5DIQrDoBnGBJJewmeUnI0RikSu9oHWe8jqVs4JicSRoY0YglIn9nPJWfSX5zyV0ktX4IOiXSVKlZaBpqlxkE0tcDTgcwfAzZY5LThGzcc9CqtZLbyaXF7k5G4/XCdqw+ParTR3ADsiswF7AkA2sc54OfE4yTXDPVxzuLTHU1uwB5geZmjGuX7x9BKDCZug+8PQy/U6f4j6zLl5RRA2LxaU0LucgNOZJNgOZmK2j02xCXdaFPdB0LMzZm3JRF+0TazU3o01tkhdgeNzZfg3nMliNpCpT3d0h7g21BA5ekphxWlJrYnLIlaXJv+j/TilXIVx7Nzob3QnkT9nx85sVe84rg8BvUA4yO83yHllN90L2ozp7NzdkHVJ1KjIqeZU8eXdBOKT2Gj7ops1Di5/X3pA+Ey6vl+R4fCTMdf1wMqtubcXDrku+5Fwl90Ac2Nj8JHTqdDW1wBbZ2WlZCjXRsjvAZi2lxxH6vC+j2ENKmiMQSq2uND1rgjwMpdmdM6dVxTroKZb3WDFkvyJsCp7flNM+HK5rmPXxHHvE6anGk+EPFxfHcB2vhwzqTwW3qY2kgtoIRiV3gDyFpCk9Xp8urEknwY8uPTNtrkIRxEFUnQd8buCPVgBLKT7CUMxWz1YWlLR2CyksW/OXb1e2DvXy7JfHkmlSIzxxk7ZnMbRcE7xy0AEz+ODcTcaDsmyxrrpbWU1agpm/DlrdowZsKlsmVGzMJc7xGXCaLDUgqgDSD01Cx1TFgTskpTew2KMca3LBDJlqgSmTGgC97xrY0NkDJPCyqzIv1xQg9bEQTCqW0lnRwQtdpKUVFlYyckVTveMRLwzE4UDQwFXsY8d1sI9nuT+xE9kf0iKCpHXE5lUpquJK5BVqbuZyAQ8z2j1nX6YsAOQHpOZdHdns+KVgjFd523jfMXOdzqbG86beeNna1KPhHo41s3+QzZo+sTx+Bl0NP8JPnKfZY6/crH0t85ccP8KjzmHK9y64Md0voK9UhlBAVRn3X8NZicfgkpWcXzNrcu4zb9IKh9u+VxcDI55ADMTI9IHBVAOZPaMuI4azTjdJIlkitLZX+3qp103gjeKngbjThND0P2iz1CbAFCrXHEE2bLu+MZsyn9Sg5qfUkwrYGEVaxYCxZGBtpkynSLOUXaa3DCEo009mdHJ/X+Gc36SYovianYxUdy5fKdHQ5eHyWcqxtQtiq4t7tR7fxkD0ndLHVNoOSWlFds9Q4dWGrcfiJ0ToPtJnpNSY3ekd0E6lPs37tO60x+4OWcu+iI3cS1uKWPqf6RG6qKizsT1Ro2tShqR4jw1HbAMQlusNP1nLZGtr+spDXpXzXxHzEzwk4O4jtKSplL9KFidLMV77BTf8AzekhfGCGLs0MXTesGF1+645DiM+cxdbFuCRfQkeRnudDp6hOuV5PN6ubwNX3NBUxw5wTFY8buvdKCrVJGciZzoZ6sOlSPLn1r4DXxzEWvI1xaqM7kwEuYwzQsUTK+pkFPjSTI2xHG5vIktxF+zP5SxwmBp1clbcPJjlC4xjvQqnOe17lYzkm8elRiwAyljidgup6hVxa+Rz8pXbjKcwR3i0KcZLY5qcXuabZ7gC9zlCa+0rDWZpcYQNc54MUDkw1mV9Pbtm1dVFKky1xO0CRlzleMU18xlzgm+ActI2tUJ00lY4UtqIy6hvewz6T2ieyrij+iif3MjW4HY1Okd5bl/3mNzy4ZSwUzKVulKjdIsd5b2GZB7eEnobbL7o3TYjPs8Z8ZJS5Z9amuDabJ95zyS3mR+UuTx71HzmV6K4xWaopNsltkTkNSTzuRNMhJOoIve47uUyzlcqKJbGT2qt6jn7zfGZHpOPcH4vlOt1sIr6geIvM7tnolTrWIZkIBAtmM+YP5xoTcZWxci1RaXJl9noRSSx+wuRGWnmIXsVj7XMaIx5jVZZvsR0QKBvbqgXHYLXsYPsqgVqHeUghCM8tWHDwnKacmPpqKNso0/X7s5vj6QNZzxLVP550n8/6hOd41Ou3+IjvvNXRf7SGb4MgfDncYgi4VrcM7awjoI9RsQ2/e6i3f1Xz7ZDum2fd52lt0ZUDEsB+78nlet9qS8i9Ort3wXPSfaho0wFazvkpGoAA3mHbmB4zA9H+kuJpPvM71E3yrK7Fj3gk5H9GaD9oeznrPhlQ2YCpbyQ68NJz/D4l6YIK7yljc9uXHn2GJixxeP8AIJ5HGVPg7crq6rVQ3VhcHsPZw7RMVtvZ6oxdHBVm0vcqTc+IyOc0HQ3E7+HHI2Yf4h1h5g+czW0qf11TL7b/AMxmv6RCXrtxdJcquTJ9TlH0kmrfZ+CtKxhSFbkS0xxn06Z8642BlI0pDTS5Azz2JEKkK4AW5EUhpTsjqeEZtAZ2pLkCg3wDJiai6Ow8ZK2Mdve63eBLdtiqBcub8dIDWwdjkQZKOSDexolhyxW7/kq6gub2tG7sONHOO+iX4iU1JEPTk2V25FaHNQjPZdkbUhfTkB7sUsPoJ5xQa15D6U/ADR2fSTPdF+bZ+Rkr4+kg98eGfwmPd3bMkntJJPrN7srZFNNxtwb1k3iczf2e8Tc6a8J8Vkw6Fcm2fawy6tkqLnoniKe4WZG656r2t1eIByOo4TUIFa269u/+9jIthUQtCmCPsMfMwh8ApsFAU7o0uBfLUDIzBJJyexXUOvUHaPP42McMXbJgR+uRgRw9Zfdvb7rA+jWkdXaDrkygj7wK38TkYFt3Gq/Bbiqp0Ija1FbXtKtMZTPvK6H09Lj0hVMXHVcN2X/L8p2p90mLVBe98f6pz3Esd4kggeY1PEaeNpumdhqt+0f2mXxeyXs27ZsjproeBlcObRK3/J0oao0VAztY3BI00Octuji/7034B8HgXRzZO6jmqpVg+W9dWyVTkcja8tdgp/vLEm5KjW33uQl+ozKaVE8MWk2wvpE+69FrE6rln7zU+HHIHSZ/oTQSpRqq6q31rZHPIok0+3MG7tSKC4RwWz4dX+8oeh2z2SnVWohVvasRvAqbbiZqfDURdS0PfcVxbyLxuXnRqgqIyoLBXqADkBUbKZ3aifXVP+Y38xmm2Atgwz9+pqb/APeNxlHtT/iv+M/Ger9Hl/kl+kYPqkfYv2VW5FuQndnvsjyn0Gqjw9NnlIC2Q63OL6LfTU856oMmpq0SUnHdFYQjLZip7NzzMLVQnbPVD27JE9MnUjzmbVOTpmpRhBWkD4pyTlB6VIatJXBjCk0Ri9NGac05XyNqEcNJAByhIpzw05SKS2Jybe4PaSq9uEk9nF7OFxT5ApNcEftIo/2cUGiIfUkYf6MMhzy850F1963KoR3qip8ZkMLTvVpjm6fzCbJFv538KlX8hPleqfCPpcC5NZhVsgHJB63k7MFJPYBl4xirZbfgHwg+08elBHqPoLWHFmtko7TPNe7NAU9ewJayga3OnedJXjbmFZgv0miWHD2iXvYjS85ttrblbEXZ2sgOSLoNRYjiRzmTwWHDubgaMcuzd046kzVj6bVFuT4JudNJI7+2FptmVGee8uRPiusgq7OUEa2Pdl3G15yfYu28Rg3AVi1O9ijE7ufD7p+8PG86zszaCYimlRD1ScwdVPFT2iQyYdP6KRnYvorr7tQ8MmAbnlfK0HfHsv8AxKWXaCPXMestL5eXwMr9p7Zo4dQ1eoEvooBYnuVQWPfpJqLeyGvyRpi6LfaK9+Y88xH0MCqvvpusSLGxtl3afCCbO23gcU1kZGfkylXPdvAE+EsWwKXsN4cbhjceMEoOLpqv4CpXwTU69sipH68pJdW4iCCjVX3W3x94i/otjGvWYe9SPaR/+bzr2oFJhNLDKh6uV7nxJufWZnaafWv3y/XEJrv7udutbXW3fBcbs4NvOGz1yII8p6f0zqI4sr1d1XkxdfheSCrs7KDck6Ur6tCBs9+X68Z4uFa4FiJ9I8ilw0eKoSjzFjlw6DkY0lAcgIY+yGAvvA9ljBqmEZdRIwcW/lZaepLaNEbsp/RjPZr4x6py+E8btvlK8OrJfJXRG1EHkInwjDUEeEYzrcHetYgixzJGefZLKnuHrEC3K58hcmw7ok87i6SseHTalb2Kw0Y0paSVcUEqbjjqMLq4+yc+qw45cR68HLjaDEAOtzpe49TKRzxfclPBKPYHYCQvVtlYnulmwp3tvJflvL+ce2HHKU12S0V2Kb2p/caKWv0deyKCw/8ADH7HW+Ip/iv5An5TU4RL7l9SKS+IV3mY2Pf2jMNUp1GHeFsPjNngk+tVf/UBHctNV+c+X6nlH0mH4mk/1fD/AKTEftNxbJTpBTq7tawN91VA175uF4fiJ+MxfTygrsgYXsrkdm84HymLFJKacuCsk2qRzX6c53r2IIzy0PA9k8wFUI193LMWvzI/L4Q/F4FArECxAPmDYSDZVBal1J3dxbggC5JJ19B5T01KDi2lsZ3GSkle5PVxaOLENnloLfGbb9ntJ0AbeBSoNM73UkBvEA+YmOxOybkbr3z1I4gi1zedA6I0t2nSX91W8+veZsrho9rLRUr3NNiK4RGdtEUse5VJnH9t4tqre1cK5dtCTYWzC9U6C/61nUukl/ote2vs3A8UsPjOJVabCwc7t+J7IvSwTtt8C5ZNB2NwhTddOqy2NxcZDRhxBB4/GdU6I7XOJoK7H6xOo/acrNbtHrecgr1nAALta1s76f4uy+U6J0BWzEi9npqxHbdM/U+cPUqoq9xsTu6NbtHHpRRqjtuooubak8hzMzOB/aFRdwHpOiNo5Ib+JRp4XgP7Sdo7j06RBKsjnW1ierfwtr2mY7CYul7MI+9fOxtob3v63vaDFguGpq7A5+6jtwZWzFirAEcQQePaJz7F9LFpV6lNaYG41YdUAZU/dGXOxl/0GqsaAV72GaE/uNwHYD8ZyfpQ7fTMUL61qi+G+Tbuj9LGptC5n7UdC6PdJzWVVYgVN7d1sGOot2kA5dkk2p0mFOslNyvXBJO8OrrYnsuLTk6VmSxU2YMGH4hocvONctfM3OWZJPvZ8e+/fPQUqRladnTKPTtSjkDdKhSATe92UHxG8fKRbU6fr1VRSQb79+AG8LAjjcKe4zG4fYbOiMrgb6sRvDioF1BHPh+GU29l3/OBSC0+5tMF01ff665db3fDdHblfzh2K/aB1LBLHfAzz6gI3r8LkXHjOfq9vAfC8jqkkX7fjG1t8i6UuDQV+klV7sN1Bkd1eAFtCfGRU+kdZG3gxa5JINyNABYcP7mVCE53HDwkTj9d07VJvk7SjVVOlrubOu8Du5G1h7293/ZlJidouS3AZWzNhZgwtn4ecr0rW1j/AG18rZTm5B2LKltYEgsgNr5jJrHTsykjdI6yW9nUdcrkXuBmbKAciLW4SsUgmR1KYGfDO3Md8MckltYrhF9gr/8AocT/AOMf4V/0xQG3dFH9SXkGiPhHTejtO7VTyVE/9yoB8prtli9ZO+sf/kFvQTN9Fk6rn96pTX+Def5TT7Fzfe5Ulb+MuTPI6h+5m7EvaXqfZ7ifh+cwXTSoDXRSTlTTQE+8zm83py8F/XwnMumtU/TG5KqD/ID8/SZsSuRVsz+0n+rbXTWxGrjPMSt2bXtvWIz3fW3rC9qOfZm/HdHxME2ZXVQc8rjXnYXM9GC/xv8AZCT96/Rc4CsLm7DUcRxa/wAp0Doz7lM80J/iBPznNE2mq3yvofjzm36L7XLKihAN2kMz2BBM88babSKqSNN0me2GrH7p+KicZ2299wfi+U6P0yxrfRnJzzGXD/iJ+U5Xj8WGKXFsz62lOmxOLsllkqo0rsCADwHyE1fQEe7ll7Fbd28tpiUxKjNr2y0P95uehDgmy6LSUDjkN2Z+oVJL8l4cP9AH7Sqal0JAuKbWPLMzH7Mwi1FG/wA+7TIaTcdNkD4zCo2au1NWHAq9bdYeIyk/STYOHw6I1FAhapY5sRmpOhJtmBKwyaYJdybjcrLjoogWjTUaBLDwa05P0ro2xmIa2Xtj5kk5+F/Kda6OD6tPwn+Yzl3THCscVibAZ1lIz+4fznYJe92wZV7UZmuhv4D1Vc/GS4hLC/YluGe4hNh4z0YV+JHieVsvT0hOIwrFDpb6sXzsOog5c1PlNuqPkz0ybC4mmEpAHdcM29d3UbpV7Zi4F+qLgHXSVAABIJGh7b2NrA8eJ8JMuF0u6gZAWBOumckw2GUluuTdG4E2sVN7eEKkgEK02YhUUsSDkqliSCdAMzlHrgmDqjq6bzKDvKRa5AvY980Oydsrh1oomIcXqqai7oC7vtLN1sm90jK7DXITTba6V4F9xfpJ6jqxCIzBwCCVJ92xt6mK5ST2QyinyzmlTCsmRVhvXtca2yMjqrbd1zJ07N3j4mbHpL0gwNZd1aJDoCCQiJc3IJBU8MmFxwmO9qgAAFzYb1wPeub2JHEW8o0G3u1QsklwyBqY3N7jv28Cptl3qZEnjLA+zswNrBgxtl1esN0c82HlI6jUbAoGB61wTfUWFsuBz8eyUsFEK3ktWoLWMiVkvxtft0vz524xpZSb9n2rnPw4QadwD8v0TFG76808niho4650f6tDe+/UfwSnu/OabY6WNQfuqieSKf6pjMFtNUoqm6SdyoDwH1jqB6LL7Ym1Xf2hsBd0/wBP9M8zLCUm2bISSSRrahHW7h85xjpntIHG192xAfd/hVVPlYzpdWozb1yT1lH8v5ziW2cQTia51vVq/wA7R+mwJNti5J0lR5tLFsygEi1xkByB7YJhzlrx/XGMxdS9r2klEgJe41M3pJRozt6pWPVzbUacuw9s6R0SXPupqP8AMn5TmiPceendOpdGEI3ifufzf2kcvBSHJ703e2FbtYerk/KcsxJJZf1ynR+n1cLhlv8AaqJ8Kh+U5nVqgsp5WjYVtYMgbUc/of3nQP2fY9g73AP1Y+U5rVxIM3nQQ9Z/wD5/lJ54JrdDY5buiy6c7Q3cVQqKt/ZBH3SbX3KjNu34X3dbRYrpecaiqKO4Va/v74PVItko538JH0s2DXxFVXpblhT3bs27nvOeR4EQbYXRTEUxZ2pDtDMdf8Mk8cVDblDxk9RvOjZ+qT8LfzGcz6a40Lja6Ze+h1I0RTw01nR9l11oqtNzdgrZre2bE8QDxnIenVQNj8Qbasn/ANaRenhqyO12DmlSVAq7SVdAved4/OT/AO3yBkqm9r5Nwvpc9spTaeZTY8MHyjNrZaYjbW+LMinw07BnlK/6QLjqiwN7ZnLlnPKKBjbsJ8heI01jxxxjskK5N8klRlVnWwyLWOdwVOXHja3jIHq3+ysnYBixP2rnLt1jzuEk7mp4G3bllGpAsENS5JIFznpzni1CAQLWIsctcwfiBOg4Ho7gqmzziNxvaim9vrDYum8BZbjUqMhzmKbD55U27rNEjkjJtLsNKDVfkDV8+zK/aL5/CWGFak7hStgcr8ezIDODYnDMuRQplfrAqSDoRvcJAFA+1mP1qI0lqQFsaqvsemEDKRmPDu520zPZKysKS5AZg558OY1z9PnDT2gCtnZzmLhbAFftDPQ8tR2ZCCtUTgCPX4yEMcltJtjyceyJ/bJ97yE9g3tB+gJ7KaP2IbeiunaKfxLTTdGV6hP7z/C7fOUeMqUkGRzVqYy5NSGf8RMtuitfeooR++/+UBflMrlqVmiqL0n1qfD/AKQD/ZWHuT7CjcneJKKSSeJ7e2SvVsgP42/m/OZkbSqWyUFieJOncO6JFtcDNGoTAUhpTpjuRR8pItJRoFHcFHylHS2swsGBJOtrkDn3SSpjmtrroBr6Q6waS5UgfoSv2ewLMebD4OYIcYwYXJseV/CN2VVJDEWyb+k/nO1WGqKj9pLf7tSH309EqfnOc2+U3n7RHPs6IJ+0dBb3VI+cwioeR8psw/BGfJyeEazoPQh+vV/AP6pghRblNZ0Z2qlBnLqzBwtgADmCdbntgzK1sHG6e50lKw58T8TE+IHP4zG4jpNfNKZtyY2t5QKv0irNpuKPM/H5TNplwWtGrOK+ute/V4d0530rW+Lqm2pT+RIYdsup3mq52tew8eEAxG0qLsWcszHU7ozsLDgOQErii4uxJtSVFWV7Iw9xhb4qlwQ+JPytIHxA+ypHifneaFL8EXEjViD7p48+ItPC/OJ3ZuwfrjI9yGwUSF1HGN9qOAJ9J4E5CPFI8p1hoXtzwEfh8fVRw6OyMuhU2InqYeSDDRW4hSZDicXUdt53d203mJY25XbhnGC/IeQhX0YR4pgQ2lwdT7gyg9nkPyjwexT4D8pMVjSonWjtxvtPup/An5TyO3RFO2Otl/j8bVJ3tynnbT7oPAPkRnwms6OIfo1O4AuGawuAN5ibayj/ANrIw66IWOVt0HI9p4Zeol1st6NbdQIANOq7KR27oynmOajGmmjSvdK0w7EuiIxY26pHbnylBS2xhuN9LZq1rjiLC8s8b0U3esrFxya294cGlNWwqISHBU8iLHyMeGlrY6SkuR1TpChPUoEgZAnIdndHU9uv/wCX63ElwB/LA3roNGJt2QN9qKRy77SqiuyFb/JZ1dq4g5Baag8rn+0Hp1aqruh90E3Nhx01lPW2uvMdmV/hAqu1mOgPj+UZY2+wrkkaDEUd+3tHZraXIyvyyygRp4dT1nPdcfLOZ+tiXf3mkW7KLG/IrmvBoG2pQUdVbwdtqFjuogJOgtKgCekRlBA1MLqbRc8h2QZ6ztqxPjEqxRkkuANtjQkd7Oe3ijAPAI6eR60yYp1DI+numSInZHonYBObOocEXlHgRZRXiDjgYi0YZ5CAcWnkbeMZoQWPYxhaNERhoFjrz2MuOyKccXdTR++WfRL/AIh/EPiIop5+X4spj+SOl4bRe75iZf8AaJ/3P4X/AKZ5FIYPmjVl+LOcV9fH5ytxHz+cUU9VGJjV0iMUUohWeJHxRTgnsQiigOEYuMUUBx6J4NIooTkSppJuE9iihHLHRRRRjxooooThTyKKEA06SMxRQoU8M8bQxRTjgeKKKE4//9k=" alt="logoEQR" width="100" height="100">
   </th>
   <th  width="1%"></th>
   </tr>
   <tr>
       <th></th>
   </tr>
   <tr>
           <th rowspan=2></th>
         </tr>
         <tr>
           <th></th>
         </tr>
         <tr>
         <td rowspan="5" class="bordeGeneral textoInformacion">
           <span class="textoGrande">JUNTA ADMINISTRADORA DE AGUA POTABLE BARRIO EL MILAGRO</span><br><br>
           <b>Dir Matriz :</b> Pastocalle - Barrio Milagro<br>
           OBLIGADO A LLEVAR CONTABILIDAD &nbsp; SI
         </td>
         <th></th>
       </tr>
   </table>
   
   <table width="100%" cellpadding="5" cellspacing="0">
<tr>
  <td style="width:20%"></td>
  <td style="width:20%"></td>
  <td style="width:20%"></td>
  <td style="width:20%"></td>
  <td style="width:20%"></td>
</tr>
<tr>
<td colspan=2 class="bordeGeneral textoInformacion" style="border-style: solid hidden hidden solid">
  <b>Usuario : </b> ' . $Nombres_completos . '
</td>
<td class="bordeGeneral textoInformacion" style="border-style: solid hidden hidden hidden">
  
</td>
<td class="bordeGeneral textoInformacion" style="border-style: solid hidden hidden hidden">
 
</td>
<td class="bordeGeneral textoInformacion" style="border-style: solid solid hidden hidden">
  
</td>
</tr>
<tr>
  <td colspan=2 class="bordeGeneral textoInformacion" style="border-style: hidden hidden hidden solid">
    <b>Direccion:</b> ' . $Direccion . '
  </td>
  <td colspan=3 class="bordeGeneral textoInformacion" style="border-style: hidden solid hidden hidden">
    
  </td>
</tr>
<tr>
  <td colspan=5 class="bordeGeneral textoInformacion" style="border-style: hidden hidden hidden solid">
    <b>Telefono : </b> ' . $Telefono . '
  </td>
  
</tr>
<tr>
  <td colspan=2 class="bordeGeneral textoInformacion" style="border-style: hidden hidden hidden solid">
    <b>Lectura Anterior</b> ' . $lecturas_anteriores . '
  </td>
  
</tr>   
<tr>
  <td colspan=2 class="bordeGeneral textoInformacion" style="border-style: hidden hidden hidden solid">
    <b>Lectura Actual</b> ' . $lecturas_actuales . '
  </td>
 
</tr>  
<tr>
  <td colspan=5 class="bordeGeneral textoInformacion" style="border-style: hidden solid solid solid">
    <table width="100%" cellpadding="5" cellspacing="0">
    <tr>
        <th colspan = "4" class="bordeGeneral textoCampos">CONCEPTO PAGO DEL AGUA DE POTABLE</th>
      </tr>
      <tr>
        <th class="bordeGeneral textoCampos">Fecha de Pago</th>
        <th class="bordeGeneral textoCampos">Acometidas</th>
        <th class="bordeGeneral textoCampos">Excesos</th>
        <th class="bordeGeneral textoCampos">Total</th>
      </tr>
      ' . $fechas_pago_1 . '
      <tr>
        <th colspan="3" style="text-align:right">TOTAL A PAGAR</th>
        <th>' . $total . '</th>
       
      </tr>
    </table>
  </td>
</tr>
</table>
   ';
    $mpdf = new \Mpdf\Mpdf([
      'margin_left' => 10,
      'margin_right' => 10,
      'margin_top' => 10,
      'margin_bottom' => 10,
      'margin_header' => 10,
      'margin_footer' => 10,
      'showBarcodeNumbers' => FALSE,
      'default_font' => 'dejavusans',
      'format' => 'A4'
    ]);
    try {
      $mpdf->WriteHTML($output);
    } catch (\Mpdf\MpdfException $e) {
      die($e->getMessage());
    }
    $mpdf->SetJS('this.print();');
    $mpdf->Output();
  }


}
