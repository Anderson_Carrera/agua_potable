<?php
include("../../../../conexion/conexion.php");
$sql = "SELECT Id,Cedula,Nombre,Apellido,Direccion,Telefono,Correo,Estado,Nombres_completos FROM usuarios";
$resultado = $obj_conexion->query($sql);
$return_arr = array();
while ($row1 = mysqli_fetch_array($resultado)) {
    $return_arr[] = array(
        "Id" => $row1['Id'],
        "Cedula" => $row1['Cedula'],
        "Nombre" => $row1['Nombre'],
        "Apellido" => $row1['Apellido'],
        "Direccion" => $row1['Direccion'],
        "Telefono" => $row1['Telefono'],
        "Correo" => $row1['Correo'],
        "Estado" => $row1['Estado'],
        "Nombres_completos" => $row1['Nombres_completos']
    );
}
echo json_encode($return_arr);
