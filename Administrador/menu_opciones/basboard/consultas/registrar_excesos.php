<?php
include("../../../../conexion/conexion.php");
date_default_timezone_set('America/Guayaquil');
$arrayexcesos = $_POST['arrayexcesos'];
$id_fecha = $_POST['id_fecha'];
$fechaComoEntero = strtotime($id_fecha);
$anio = date("Y-m", $fechaComoEntero);
$Estado = "1";
$sql = "SELECT Fecha_Ingresada FROM exceso  LIMIT 1";
$resp = $obj_conexion->query($sql);
while ($row = $resp->fetch_assoc()) {
    $fecha_1 = $row['Fecha_Ingresada'];
    $fechaComoEntero1 = strtotime($fecha_1);
    $anio1 = date("Y-m", $fechaComoEntero1);
}
if (empty($id_fecha)) {
    echo json_encode(["message" => "Seleccione la Fecha de Ingreso"]);
} else if ($anio == $anio1) {
    echo json_encode(["message" => "Ya se encuentran registrados los excesos para este mes"]);
} else {
    foreach ($arrayexcesos as $key => $v) {
        $rdEQR = "INSERT INTO exceso(Usuarios,Fecha_Ingresada,Lec_anterior,Lec_actual,Exceso,Estado)
        VALUES (
            '" . $v['id_usuario'] . "', 
             '" . $id_fecha . "', 
            '" . $v['lec_anterior'] . "',
            '" . $v['lec_actual'] . "',
            '" . $v['exceso'] . "',
            '" . $Estado . "'
        )";
        $conec10 = $obj_conexion->query($rdEQR);
        $update = "UPDATE usuarios SET L_anterior = {$v['lec_anterior']} WHERE Id = {$v['id_usuario']}";
        $conec11 = $obj_conexion->query($update);
    }
    echo json_encode(["success" => "Transaccion Exitosa"]);
}
