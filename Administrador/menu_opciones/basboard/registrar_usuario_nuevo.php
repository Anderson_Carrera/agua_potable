<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>

<div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">
                <div class="container">
                    <button type="button" class="btn btn-info fa fa-plus" data-toggle="modal" data-target="#myModal">
                        Nuevo Usuario
                    </button>
                    <hr>
                    <label>Listado de usuarios registrados en la Junta de Agua potable del BARRIO EL MILAGRO</label>
                    <hr>
                    <table id="tbUsuarios" class="table table-striped table-bordered" style="width: 100%;">
                              <thead class="bg-secondary">
                                <tr style="color: #fff;">
                                  <th>Cedula</th>
                                  <th>Nombres Completos</th>
                                  <th>Direccion</th>
                                  <th>Telefono</th>
                                  <th>Correo Electronico</th>
                                  <th>Estado</th>
                                  <th>Editar</th>
                                </tr>
                              </thead>
                            </table>
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Nuevo Usuario del Agua</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Cedula <span style="color:red">*</span></label>
                                                <input type="text" id="cedula" class="form-control" placeholder="Numero de Cedula" maxlength="10" minlength="10" pattern="[0-9]{10,10}" onkeypress="return valideKey(event);">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Nombres <span style="color:red">*</span></label>
                                                <input type="text" id="nombres" class="form-control" placeholder="Nombres" onkeypress="return soloLetras(event)">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Apellidos <span style="color:red">*</span></label>
                                                <input type="text" id="apellidos" class="form-control" placeholder="Apellidos" onkeypress="return soloLetras(event)">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Telefono <span style="color:red">*</span></label>
                                                <input type="text" id="telefono" class="form-control" placeholder="Numero de Telefono" maxlength="10" minlength="10" pattern="[0-9]{10,10}" onkeypress="return valideKey(event);">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Correo Electronico <span style="color:red">*</span></label>
                                                <input type="email" id="correo" class="form-control" placeholder="Correo Electronico">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success fa fa-plus" id="btn_registrar"> Registrar</button>
                                    <button type="button" class="btn btn-danger fa fa-times" data-dismiss="modal">Cerrar</button>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<script>
   

    function soloLetras(e) {
        var key = e.keyCode || e.which,
            tecla = String.fromCharCode(key).toLowerCase(),
            letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
            especiales = [8, 37, 39, 46],
            tecla_especial = false;

        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function valideKey(evt) {
        // code is the decimal ASCII representation of the pressed key.
        var code = (evt.which) ? evt.which : evt.keyCode;
        if (code == 8) { // backspace.
            return true;
        } else if (code >= 48 && code <= 57) { // is a number.
            return true;
        } else { // other keys.
            return false;
        }
    }
</script>