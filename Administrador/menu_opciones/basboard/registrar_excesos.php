<?php
    date_default_timezone_set('America/Guayaquil');
    $Fecha_Actual = date('Y-m-d', strtotime('-1 Month'));
?>
<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>

<div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">
                <div class="container">
                    <hr>
                    <label>Ingreso de excesos del Agua potable para el cobro del mes de <?=$Fecha_Actual?></label>
                    <hr>
                    <button type="button" class="btn btn-info fa fa-floppy-o" id="btn_excesos">
                        Ingresar excesos
                    </button>
                    <hr>
                    <div class="row">
                    <div class="col-md-2 ">
                    <label>Fecha para el Cobro : </label>
                    <input type="date" class="form-control" id="id_fecha" value="<?=$Fecha_Actual?>" readonly>
                    </div>
                    
                    </div>
                    <hr>
                    <table id="tbexcesos" class="table table-striped table-bordered" style="width: 100%;">
                        <thead class="bg-secondary">
                            <tr style="color: #fff;">
                                <th>Codigo</th>
                                <th>Cedula</th>
                                <th>Nombres Completos</th>
                                <th>Lectura Actual</th>
                                <th>Lectura Anterior</th>
                                <th>Exceso</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>