<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
<?php
date_default_timezone_set('America/Guayaquil');
$fecha_Hora = date('Y-m-d');
?>
<div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">
                <div class="container">
                    <hr>
                    <h1>RECAUDACION DE DINERO DE MINGAS / SESIONES / AGUA DEL <br> BARRIO MILAGRO</h1>
                    <form action="menu_opciones/basboard/consultas/generar_comprobante.php">
                    <div class="row">
                        <div class="col-md-2 ">
                            <label>Ingrese la Fecha Desde : </label>
                            <input type="date" class="form-control" id="id_fecha1" name="id_fecha1"  required>
                        </div>
                        <div class="col-md-2 ">
                            <label>Ingrese la Fecha Hasta: </label>
                            <input type="date" class="form-control" id="id_fecha2" name="id_fecha2"  required>
                        </div>
                        <div class="col-md-2 ">
                            <label>Buscar</label>
                            <button type="submit" class="btn btn-info" id="btn_multas_mingas">
                                Generar Comprobante
                            </button>
                        </div>
                    </div>
                    </form>
                    <hr>
                    
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>