<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-4 col-sm-4  profile_details">
            <div class="well profile_view">
                <div class="col-sm-12">
                    <h4 class="brief"><i>Usuarios Registrados</i></h4>
                    <div class="left col-md-7 col-sm-7">
                        <h2>Numero de Usuarios</h2>
                        <p><strong>Registrados </strong>
                        <div id="cantidad_usuarios">0</div>
                        </p>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-eye"></i><a href="registrar.php">ver....</a></li>
                        </ul>
                    </div>
                    <div class="right col-md-5 col-sm-5 text-center">
                        <img src="img/icono.png" alt="" class="img-circle img-fluid">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4  profile_details">
            <div class="well profile_view">
                <div class="col-sm-12">
                    <h4 class="brief"><i>Acometidas Usuarios</i></h4>
                    <div class="left col-md-7 col-sm-7">
                        <h2>Numero de Acometidas</h2>
                        <p><strong>Entregadas </strong>
                        <div id="cantidad_acometidas">0</div>
                        </p>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-eye"></i><a href="vender_acometida.php">ver....</a></li>
                        </ul>
                    </div>
                    <div class="right col-md-5 col-sm-5 text-center">
                        <img src="https://cdn-icons-png.flaticon.com/512/6152/6152273.png" alt="" class="img-circle img-fluid">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4  profile_details">
            <div class="well profile_view">
                <div class="col-sm-12">
                    <h4 class="brief"><i>Multas de Mingas Valor $10</i></h4>
                    <div class="left col-md-7 col-sm-7">
                        <p><strong>Dinero por Cobrar </strong>
                        <div id="cantidad_multas_mingas">0</div>
                        </p>
                        <p><strong>Dinero recaudado </strong>
                        <div id="cantidad_multas_mingas_cobradas">0</div>
                        </p>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-eye"></i><a href="mingas.php">ver....</a></li>
                        </ul>
                    </div>
                    <div class="right col-md-5 col-sm-5 text-center">
                        <img src="https://w7.pngwing.com/pngs/528/674/png-transparent-fee-money-payment-service-bank-bank-company-service-payment.png" alt="" class="img-circle img-fluid">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4  profile_details">
            <div class="well profile_view">
                <div class="col-sm-12">
                    <h4 class="brief"><i>Multas de Sesiones Valor $20</i></h4>
                    <div class="left col-md-7 col-sm-7">
                        <p><strong>Dinero por Cobrar </strong>
                        <div id="cantidad_multas_sesiones">0</div>
                        </p>
                        <p><strong>Dinero recaudado </strong>
                        <div id="cantidad_multas_sesiones_cobradas">0</div>
                        </p>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-eye"></i><a href="sessiones.php">ver....</a></li>
                        </ul>
                    </div>
                    <div class="right col-md-5 col-sm-5 text-center">
                        <img src="https://img.lovepik.com/element/45004/5444.png_860.png" alt="" class="img-circle img-fluid">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4  profile_details">
            <div class="well profile_view">
                <div class="col-sm-12">
                    <h4 class="brief"><i>Excesos del Agua Potable</i></h4>
                    <div class="left col-md-7 col-sm-7">
                        <p><strong>Dinero por Cobrar </strong>
                        <div id="cantidad_excesos">0</div>
                        </p>
                        <p><strong>Dinero recaudado </strong>
                        <div id="cantidad_excesos_cobradas">0</div>
                        </p>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-eye"></i><a href="excesos.php">ver....</a></li>
                        </ul>
                    </div>
                    <div class="right col-md-5 col-sm-5 text-center">
                        <img src="https://www.blueindic.com/wp-content/uploads/2017/05/facturasefectivo-01.png" alt="" class="img-circle img-fluid">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="right_col" role="main">
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="dashboard_graph">
                dfdffd
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div> -->