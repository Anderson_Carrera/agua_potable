<?php
session_start();
?>
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.php" class="site_title"><i class="fa fa-tint"></i> <span>Agua Potable</span></a>
                </div>
                <div class="clearfix"></div>
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="img/logo.gif" alt="..." width="200px" height="70px">
                    </div>

                </div>
                <br />
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-users"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="registrar.php">Nuevo Usuario</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-tint"></i> Numero de Acometidas <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="vender_acometida.php">Asignar acometida</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-book"></i> Ingreso de Multas <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="mingas.php">Mingas</a></li>
                                    <li><a href="sessiones.php">Sessiones</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-table"></i> Ingreso de Excesos <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="excesos.php">Excesos</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-bar-chart-o"></i> Generar Cobros<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="cobro.php">Cobro del Agua</a></li>
                                    <li><a href="cobro_mingas.php">Cobro de Mingas</a></li>
                                    <li><a href="cobro_sesiones.php">Cobro de Sesiones</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-clone"></i>Reporteria <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="recaudado.php">Dinero Recaudado</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    
                </div>
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="salir.php">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                    
                </div>
            </div>
        </div>
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                    <ul class=" navbar-right">
                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:;" class="user-profile " aria-haspopup="true">
                                <img src="img/icono.png" alt=""><b><?= $_SESSION['Tipo']; ?></b> : <?= $_SESSION['Nombres']; ?>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <?php
            include("basboard/registrar_excesos.php");
        ?>
    </div>
</div>