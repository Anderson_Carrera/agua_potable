<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../img/logo.gif" type="image/ico" />
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <title>Multas de Mingas</title>
    <style>
        .loader {
            position: absolute;
            border: 16px solid #f3f3f3;
            border-radius: 350%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            width: 320px;
            height: 320px;
            z-index: 99;
            top: 25%;
            left: 50%;
            display: none;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>

<body class="nav-md">
    <div class="loader" id="loader_div"></div>
    <?php
    include("menu_opciones/registro_multas_mingas.php");
    include("menu_opciones/footer.php");
    ?>



    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <script src="vendors/nprogress/nprogress.js"></script>
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <script src="vendors/gauge.js/dist/gauge.min.js"></script>
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="vendors/iCheck/icheck.min.js"></script>
    <script src="vendors/skycons/skycons.js"></script>
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <script src="vendors/DateJS/build/date.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="build/js/custom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
        $(document).ready(function() {
            lista_usuarios_Multas();
            $("#btn_multas_mingas").click(function(e) {
                let text = "Deseea ingresar las multas de este mes ";
                if (confirm(text) == true) {
                    var id_fecha = $("#id_fecha").val();
                    var treceta_mingas = $('#tbMultas').DataTable();
                    var arraymingas = [];
                    treceta_mingas.rows().every(function(rowIdx, tableLoop, rowLoop) {
                        var data = this.data();
                        var cell1 = treceta_mingas.cell({
                            row: rowIdx,
                            column: 0
                        }).node();
                        var cell3 = treceta_mingas.cell({
                            row: rowIdx,
                            column: 4
                        }).node();
                        arraymingas.push({
                            "id_usuario": $('#id_usuario', cell1).val(),
                            "id_minga": $('#id_minga', cell3).val()
                        });
                    });
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: 'menu_opciones/basboard/consultas/registrar_multas_mingas.php',
                        data: {
                            id_fecha:id_fecha,
                            arraymingas: arraymingas
                        },
                        success: function(data) {
                            if (data.message) {
                                toastr.error(" " + data.message);
                            } else {
                                toastr.success("" + data.success);
                                setTimeout(function() {
                                    window.location.href = "mingas.php"
                                }, 1000);
                            }
                            console.log(data);
                        },
                        error: function(jqXHR, exception) {
                            console.log(jqXHR.responseText);
                        },
                    });
                }
            });
        });

        function lista_usuarios_Multas() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/listar_multas_usuario.php',
                data: {},
                success: function(data) {
                    cargarTablaPlan(data);

                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }

        function cargarTablaPlan(data) {
            var table = $('#tbMultas').DataTable();
            table.destroy();
            table.clear();
            $('#tbMultas').dataTable({
                deferRender: true,
                data: data,
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'print', 'csv', 'copy', 'pdf',
                ],
                language: {
                    "url": "https://cdn.datatables.net/plug-ins/1.13.1/i18n/es-ES.json"
                },
                columns: [{
                        visible: false,
                        render: function(data, type, full, meta) {
                            return (

                                ' <input type="text" class="form-control" id="id_usuario" value="' + full.Id + '"/>'

                            );
                        },
                    },
                    {
                        "data": "Cedula"
                    },
                    {
                        "data": "Nombres_completos"
                    },
                    {
                        "data": "Direccion"
                    },
                    {
                        render: function(data, type, full, meta) {
                            return (

                                ' <input type="number" class="form-control" id="id_minga" value="0" onkeypress=return soloLetras(event) />'

                            );
                        },
                    },

                    {
                        render: function(data, type, full, meta) {
                            if (full.Estado === '1') {
                                var Estado_carnet = '<span id="spEstado" class="btn btn-round btn-success">Activo</span>'
                            } else if (full.Estado === '2') {
                                var Estado_carnet = "<span id='spEstado' class='btn btn-round btn-danger'>Inactivo</span>"
                            }
                            return Estado_carnet
                        }
                    },

                ]
            });
        }
        function soloLetras(e) {
            var key = e.keyCode || e.which,
                tecla = String.fromCharCode(key).toLowerCase(),
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
                especiales = [8, 37, 39, 46],
                tecla_especial = false;

            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }
        }
    </script>
</body>

</html>