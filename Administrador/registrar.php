<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../img/logo.gif" type="image/ico" />
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <title>Lista de Usuarios</title>
    <style>
        .loader {
            position: absolute;
            border: 16px solid #f3f3f3;
            border-radius: 350%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            width: 320px;
            height: 320px;
            z-index: 99;
            top: 25%;
            left: 50%;
            display: none;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>

<body class="nav-md">
    <div class="loader" id="loader_div"></div>
    <?php
    include("menu_opciones/registro_usuario.php");
    include("menu_opciones/footer.php");
    ?>

    <div class="modal fade" id="myModal_dos">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Datos del Usuario del Agua</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" id="Id_usuario">
                                <label class="col-form-label">Cedula <span style="color:red">*</span></label>
                                <input type="text" id="cedula_E" class="form-control" placeholder="Numero de Cedula" maxlength="10" minlength="10" pattern="[0-9]{10,10}" onkeypress="return valideKey(event);">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Nombres <span style="color:red">*</span></label>
                                <input type="text" id="nombres_E" class="form-control" placeholder="Nombres" onkeypress="return soloLetras(event)">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Apellidos <span style="color:red">*</span></label>
                                <input type="text" id="apellidos_E" class="form-control" placeholder="Apellidos" onkeypress="return soloLetras(event)">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Telefono <span style="color:red">*</span></label>
                                <input type="text" id="telefono_E" class="form-control" placeholder="Numero de Telefono" maxlength="10" minlength="10" pattern="[0-9]{10,10}" onkeypress="return valideKey(event);">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Correo Electronico <span style="color:red">*</span></label>
                                <input type="email" id="correo_E" class="form-control" placeholder="Correo Electronico">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Estado <span style="color:red">*</span></label>
                                <select id="estado_E" class="form-control">
                                    <option value="1">Activo</option>
                                    <option value="2">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success fa fa-plus" id="btn_editar"> Editar</button>
                    <button type="button" class="btn btn-danger fa fa-times" data-dismiss="modal">Cerrar</button>
                </div>

            </div>
        </div>
    </div>

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <script src="vendors/nprogress/nprogress.js"></script>
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <script src="vendors/gauge.js/dist/gauge.min.js"></script>
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="vendors/iCheck/icheck.min.js"></script>
    <script src="vendors/skycons/skycons.js"></script>
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <script src="vendors/DateJS/build/date.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="build/js/custom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
        $(document).ready(function() {
            lista_usuarios();
            $("#btn_registrar").click(function(e) {
                var cedula = $("#cedula").val();
                if (!cedulaVa(cedula)) {
                    toastr.error("El numero de Cedula no es valida");
                    return false;
                }
                $(".loader").show();
                var nombres = $("#nombres").val();
                var apellidos = $("#apellidos").val();
                var telefono = $("#telefono").val();
                var correo = $("#correo").val();
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: 'menu_opciones/basboard/consultas/registrar_usuario.php',
                    data: {
                        cedula: cedula,
                        nombres: nombres,
                        apellidos: apellidos,
                        telefono: telefono,
                        correo: correo
                    },
                    success: function(data) {
                        $(".loader").hide();
                        if (data.message) {
                            toastr.error(data.message);
                        } else if (data.success) {
                            $("#myModal").modal('hide');
                            toastr.success(data.success);
                            lista_usuarios();
                            var cedula = $("#cedula").val("");
                            var nombres = $("#nombres").val("");
                            var apellidos = $("#apellidos").val("");
                            var telefono = $("#telefono").val("");
                            var correo = $("#correo").val("");
                        }
                    },
                    error: function(jqXHR, exception) {
                        console.log(jqXHR.responseText);
                    },
                });
            });
            $("#btn_editar").click(function(e) {
                var Id_usuario = $("#Id_usuario").val();
                var cedula_E = $("#cedula_E").val();
                if (!cedulaVa(cedula_E)) {
                    toastr.error("El numero de Cedula no es valida");
                    return false;
                }
                $(".loader").show();
                var nombres_E = $("#nombres_E").val();
                var apellidos_E = $("#apellidos_E").val();
                var telefono_E = $("#telefono_E").val();
                var correo_E = $("#correo_E").val();
                var estado_E = $("#estado_E").val();
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: 'menu_opciones/basboard/consultas/editar_usuario.php',
                    data: {
                        Id_usuario:Id_usuario,
                        cedula_E: cedula_E,
                        nombres_E: nombres_E,
                        apellidos_E: apellidos_E,
                        telefono_E: telefono_E,
                        correo_E: correo_E,
                        estado_E:estado_E
                    },
                    success: function(data) {
                        $(".loader").hide();
                        if (data.message) {
                            toastr.error(data.message);
                        } else if (data.success) {
                            $("#myModal_dos").modal('hide');
                            toastr.success(data.success);
                            lista_usuarios();
                        }
                    },
                    error: function(jqXHR, exception) {
                        console.log(jqXHR.responseText);
                    },
                });
            });
        });


        function lista_usuarios() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/listar_registro_usuario.php',
                data: {},
                success: function(data) {
                    cargarTablaPlan(data);
                    //console.log(data);
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }

        function cargarTablaPlan(data) {
            var table = $('#tbUsuarios').DataTable();
            table.destroy();
            table.clear();
            $('#tbUsuarios').dataTable({
                deferRender: true,
                data: data,
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'print', 'csv', 'copy', 'pdf',
                ],
                language: {
                    "url": "https://cdn.datatables.net/plug-ins/1.13.1/i18n/es-ES.json"
                },
                columns: [{
                        "data": "Cedula"
                    },
                    {
                        "data": "Nombres_completos"
                    },
                    {
                        "data": "Direccion"
                    },
                    {
                        "data": "Telefono"
                    },

                    {
                        "data": "Correo"
                    },
                    {
                        render: function(data, type, full, meta) {
                            if (full.Estado === '1') {
                                var Estado_carnet = '<span id="spEstado" class="btn btn-round btn-success">Activo</span>'
                            } else if (full.Estado === '2') {
                                var Estado_carnet = "<span id='spEstado' class='btn btn-round btn-danger'>Inactivo</span>"
                            }
                            return Estado_carnet
                        }
                    },
                    {
                        render: function(data, type, full, meta) {
                            return '<button class=\"btn btn-sm btn-icon btn-pure btn-default on-default edit" data-toggle="tooltip" data-original-title="Modificar"><i class="fa fa-pencil" aria-hidden="true"></i>'
                        }
                    },
                ]
            });
            $('#tbUsuarios tbody').on('click', 'button.edit', function() {
                event.preventDefault();
                var table = $('#tbUsuarios').DataTable();
                var data = table.row($(this).parents('tr')).data();
                var estado = $(this).parents("tr").find('#spEstado');
                $('#myModal_dos').modal('show');
                $("#Id_usuario").val(data.Id);
                $("#cedula_E").val(data.Cedula);
                $("#nombres_E").val(data.Nombre);
                $("#apellidos_E").val(data.Apellido);
                $("#telefono_E").val(data.Telefono);
                $("#correo_E").val(data.Correo);
                $("#estado_E").val(data.Estado);

            });
        }


        function cedulaVa(value) {
            var array = value.split("");
            var num = array.length;
            var aux = false;
            var j = 0;
            var k = 0;
            var cont = 0;
            for (j = 0; j <= 1; j++) {
                for (k = 1; k <= (num - 1); k++) {
                    if (array[j] == array[k]) {
                        cont++;
                    }
                }
                if (cont >= 9) {
                    return false;
                }
            }
            if (num === 13) {
                num = 10;
            }
            if (num === 10) {
                var total = 0;
                var digito = (array[9] * 1);
                for (i = 0; i < (num - 1); i++) {
                    var mult = 0;
                    if ((i % 2) !== 0) {
                        total = total + (array[i] * 1);
                    } else {
                        mult = array[i] * 2;
                        if (mult > 9)
                            total = total + (mult - 9);
                        else
                            total = total + mult;
                    }
                }
                var decena = total / 10;
                var decena = Math.floor(decena);
                decena = (decena + 1) * 10;
                var final = (decena - total);
                if ((final === 10 && digito === 0) || (final === digito)) {
                    aux = true;
                } else {
                    aux = false;
                }
            } else {
                aux = false;
            }
            return aux;
        }

        function soloLetras(e) {
            var key = e.keyCode || e.which,
                tecla = String.fromCharCode(key).toLowerCase(),
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
                especiales = [8, 37, 39, 46],
                tecla_especial = false;

            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }
        }

        function valideKey(evt) {
            // code is the decimal ASCII representation of the pressed key.
            var code = (evt.which) ? evt.which : evt.keyCode;
            if (code == 8) { // backspace.
                return true;
            } else if (code >= 48 && code <= 57) { // is a number.
                return true;
            } else { // other keys.
                return false;
            }
        }
    </script>
</body>

</html>