<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../img/logo.gif" type="image/ico" />
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="build/css/custom.min.css" rel="stylesheet">
    <title>Agua Potable</title>
</head>

<body class="nav-md">
    <?php
    include("menu_opciones/menus.php");
    include("menu_opciones/footer.php");
    ?>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <script src="vendors/nprogress/nprogress.js"></script>
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
    <script src="vendors/gauge.js/dist/gauge.min.js"></script>
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="vendors/iCheck/icheck.min.js"></script>
    <script src="vendors/skycons/skycons.js"></script>
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>
    <script src="vendors/DateJS/build/date.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="build/js/custom.min.js"></script>
    <script>
        $(document).ready(function() {
            listar_usuarios_registrados();
            contar_acometidas_vendidas();
            dinero_sin_cobrar_mingas();
            dinero_cobrardo_mingas();
            dinero_sin_cobrar_sesiones();
            dinero_cobrardo_sesiones();
            dinero_sin_cobrar_excesos();
            dinero_cobrado_excesos();
        });

        function listar_usuarios_registrados() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/contar_registro_usuario.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_usuarios").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function contar_acometidas_vendidas() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/contar_acometidas_vendidas.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_acometidas").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function dinero_sin_cobrar_mingas() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/dinero_sin_cobrar_mingas.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_multas_mingas").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function dinero_cobrardo_mingas() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/dinero_cobrado_mingas.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_multas_mingas_cobradas").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function dinero_sin_cobrar_sesiones() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/dinero_sin_cobrar_sesiones.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_multas_sesiones").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function dinero_cobrardo_sesiones() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/dinero_cobrado_sesiones.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_multas_sesiones_cobradas").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function dinero_sin_cobrar_excesos() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/dinero_sin_cobrar_excesos.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_excesos").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
        function dinero_cobrado_excesos() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: 'menu_opciones/basboard/consultas/dinero_cobrado_excesos.php',
                data: {},
                success: function(data) {
                    //console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#cantidad_excesos_cobradas").text(data[i]["Numero"]);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR.responseText);
                },
            });
        }
    </script>
</body>

</html>